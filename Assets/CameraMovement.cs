﻿using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public float panSpeed   = 20f;
    public float panBorderThickness = 5f;
    public float scrollSpeed = 20f;
    public float minY = 200f;
    public float maxY = 500f;

    void Update()
    {
        Vector3 pos = transform.position;

        if(Input.GetKey("w") || Input.mousePosition.y >= Screen.height - panBorderThickness)
        {
            pos.x -= panSpeed * Time.deltaTime;
        }

        if(Input.GetKey("s") || Input.mousePosition.y <= panBorderThickness)
        {
            pos.x += panSpeed * Time.deltaTime;
        }

        if(Input.GetKey("a") || Input.mousePosition.x <= panBorderThickness)
        {
            pos.z -= panSpeed * Time.deltaTime;
        }

        if(Input.GetKey("d") || Input.mousePosition.x >= Screen.width - panBorderThickness)
        {
            pos.z += panSpeed * Time.deltaTime;
        }

        float scroll = Input.GetAxis("Mouse ScrollWheel");
        pos.y -= scroll * scrollSpeed * 100f * Time.deltaTime;

        pos.x = Mathf.Clamp(pos.x, 600f, 1060f);
        pos.y = Mathf.Clamp(pos.y, minY, maxY);
        pos.z = Mathf.Clamp(pos.z, 200f, 680f);

        transform.position = pos;
    }
}
