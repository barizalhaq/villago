﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapController : MonoBehaviour
{
    public Transform viewCamera;

    void LateUpdate()
    {
        Vector3 newPos      = viewCamera.position;
        newPos.y            = transform.position.y;

        transform.position  = newPos;

    }
}
